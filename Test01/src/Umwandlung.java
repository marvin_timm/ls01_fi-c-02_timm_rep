   ////////////////////
  ////Marvin Timm ////
 ////09.09.2020  ////
////////////////////
public class Umwandlung {

	public static void main(String[] args) {
		System.out.printf("%-12s|%10s%n", "Fahrenheit","Celsius");
		System.out.printf("%s%n","------------------------");
		String [] fahrenheit= {"-20", "-10", "0", "20", "30"};
		String [] celsius= {"-28.89", "-23.33", "-17.78", "-6.67", "-1.11"};
		for (int i = 0; i < 5; i++) {
			  System.out.printf("%-12s|%10s%n", fahrenheit[i] ,  celsius[i] );
			}

	}

}
