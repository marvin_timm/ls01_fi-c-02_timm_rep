package com.company;

public class Main {

    public static void main(String[] args) {
        int zahl1 = 5;
        int zahl2 = 7;

        int erg = min(zahl1, zahl2);

        int erg2 = max(8, 6, 7);

        System.out.println("Ergebnis : " + erg);
        System.out.println("Ergebnis zwei: " + erg2);
    }

    static int min(int zahl1, int zahl2) {
        if (zahl1 < zahl2) return zahl1;
        else return zahl2;
    }

    static int max(int zahl1, int zahl2, int zahl3) {
        if (zahl1 < zahl2) {
            if (zahl2 < zahl3) {
                return zahl3;
            } else {
                return zahl2;
            }
        } else {
            return zahl1;
        }
    }
}
